---
title: About
comments: false
---

I am a software engineer based out of India. I am interested in Unix and programming in general and OpenBSD in particular. This blog is a documentation of my learnings.
